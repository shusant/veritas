from googletrans import Translator
from gtts import gTTS
import os

def Nepali(text_to_be_translated):
	translator = Translator()
	translated = translator.translate(text_to_be_translated, dest='ne')
	print(translated.text)

	speech_generated = gTTS(text=translated.text,lang='ne')
	speech_generated.save("tts.mp3")
	os.startfile('tts.mp3')