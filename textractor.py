import pytesseract
#from gtts import gTTS
import cv2
import pyttsx3
from ntaa import Nepali

#import os
def textractor(image_to_extract):

	#extracting text
	image = image_to_extract

	cv2.namedWindow("News Segment",cv2.WINDOW_NORMAL)
	cv2.imshow("News Segment",image)

	
	extracted_text = pytesseract.image_to_string(image)
	print(extracted_text)
	print('######################################################')
	condition = input("Audios a-English, b= Nepali, anyother for none: ")
	

	#text to speech using pyttsx3, it is faster than gtts.

	if((len(extracted_text) > 0) and (condition =='a')):
		engine = pyttsx3.init()
		engine.say(extracted_text)
		engine.setProperty('rate',120)
		engine.setProperty('volume',0.9)
		engine.runAndWait()

	if((len(extracted_text)>0) and (condition =='b')):
		Nepali(extracted_text)


'''	
 	#text to speech using gtts
 	if (len(extracted_text) > 0):
 		speech_generated = gTTS(text=extracted_text,lang='en')
 		speech_generated.save("tts.mp3")
 		os.startfile('tts.mp3')
 		print(extracted_text)
 		print('######################################################')

'''

	
